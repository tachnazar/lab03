import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentService } from './student-service';
import { Observable } from '../../../node_modules/rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class PeopleFileImplService extends StudentService{

  constructor(private http: HttpClient) { 
    super();
  }
  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('assets/people.json');
  }
}

