import { TestBed, inject } from '@angular/core/testing';

import { PeopleFileImplService } from './people-file-impl.service';

describe('PeopleFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeopleFileImplService]
    });
  });

  it('should be created', inject([PeopleFileImplService], (service: PeopleFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
