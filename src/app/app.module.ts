import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { StudentsFileImplService } from './service/students-file-impl.service';
import { PeopleFileImplService } from './service/people-file-impl.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: StudentService, useClass: PeopleFileImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
