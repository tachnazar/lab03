import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student';
import { StudentDataImplService } from '../service/student-data-impl.service';
import { StudentService } from '../service/student-service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students: Student[];

  averageGpa(): number {
    let sum = 0;
    if(Array.isArray(this.students)){
      for (let student of this.students) {
        sum += student.gpa;
      }
      return sum / this.students.length;
    }else{
      return 0.0;
    }
  }

  upQuantity(student){
    student.penAmount++;
  }

  downQuantity(student){
    if(student.penAmount > 0){
    student.penAmount--;
    }
  }
  
  reset(student){
    if(student.penAmount > 0){
    student.penAmount=0;
    }
  }

  getCoord(event){
    console.log(event.clientX + "," + event.clientY)
  }



  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      students => {
        this.students = students;
      }
    );
  }

}
